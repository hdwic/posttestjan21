'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Course extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Course.hasMany(models.Member, {
        foreignKey: 'courseId',
        as: 'member'
      })
      Course.hasMany(models.Class, {
        foreignKey: 'courseId',
        as: 'class'
      })
    }
  };
  Course.init({
    name: DataTypes.STRING,
    address: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Course',
  });
  return Course;
};