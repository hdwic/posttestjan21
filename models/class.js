'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Class extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.Member, {
        through: 'ClassMembers',
        foreignKey: 'classId',
        as: 'members'
      })
      this.belongsTo(models.Course, {
        foreignKey: 'courseId',
        as: 'course'
      })
    }
  };
  Class.init({
    name: DataTypes.STRING,
    schedule: DataTypes.DATE,
    isOpen: DataTypes.BOOLEAN,
    courseID: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Class',
  });
  return Class;
};