'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  class Member extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Member.belongsTo(models.Course, {
        foreignKey: 'courseId',
        as: 'course'
      })
      Member.belongsToMany(models.Class, {
        through: 'ClassMembers',
        foreignKey: 'memberId',
        as: 'classSchedule'
      })
    }
  };
  Member.init({
    name: DataTypes.STRING,
    mobile: DataTypes.STRING,
    courseId: DataTypes.INTEGER,
    email: DataTypes.STRING,
    password: {
      type:DataTypes.STRING, 
      set(password ) {
        const saltRounds = 10;
        const salt = bcrypt.genSaltSync(saltRounds);
        const passwordHashed = bcrypt.hashSync(password, salt);
        this.setDataValue('password', passwordHashed);
      }
    } 
    }, {
    sequelize,
    modelName: 'Member',
  });
  return Member;
};