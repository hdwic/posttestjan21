// const Member = require('../models').User;
const { Member, Class } = require('../models');
const JWT = require('jsonwebtoken');
const bcrypt = require('bcrypt');

module.exports = {
  async login(req, res) {
    try {
      const email = req.body.email;
      const password = req.body.password;

      const member = await Member.findOne({
        where: {
          email: email
        }
      })
      console.log(member)
      if (member === null) {
        res.status(400).send({
          message: "your email is wrong",
        })
      }

      const passwordUser = member.password;
      const comparePassword = bcrypt.compareSync(password, passwordUser);
      if (!comparePassword) {
        res.status(401).send({
          message: "you failed login"
        })
      }
      
      
      const payLoad = {
       email : member.email
      }
      
      const token = JWT.sign(
        { payLoad },
        process.env.SECRETKEY);

      res.status(200).send({
        message: "your email is right",
        token
      });

    } catch (error) {
      console.log('error', error)
      res.status(500).send({
        message: "email salah"
      })
    }
  }
}