'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('ClassMembers', 'updatedAt'),
      queryInterface.removeColumn('ClassMembers', 'createdAt')
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'ClassMembers',
        'updatedAt',
        {
          type: Sequelize.DATE,
        }
      ),
      queryInterface.addColumn(
        'ClassMembers',
        'createdAt',
        {
          type: Sequelize.DATE,
        }
      ),
    ])
  }
};
