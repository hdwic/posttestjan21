'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return Promise.all([
      queryInterface.addColumn('Classes', 'courseID', {type: Sequelize.INTEGER}),
      queryInterface.addColumn('Members', 'email',{type: Sequelize.STRING})
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return Promise.all([
      queryInterface.removeColumn('Classes', 'courseID',{type: Sequelize.INTEGER}),
      queryInterface.removeColumn('Members', 'email', {type: Sequelize.STRING})
    ]);

  }
};
