'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Courses', [
      {
        name: 'Primagamble',
        address: 'jl. sudirman no 30x',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Newtrain',
        address: 'jl. Gunung Agung 100B',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Courses', null, {});
  }
};
