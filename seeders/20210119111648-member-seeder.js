'use strict';

const { query } = require("express");

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Members', [
      {
        name: 'Diana',
        mobile: '08123456789',
        courseId: 1,
        email: 'Diana@telkomsel.com',
        password: 'abc123',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Joanna',
        mobile: '085345678912',
        courseId: 2,
        email: 'Joana@mail.com',
        password: 'def123',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Sammy',
        mobile: '085234567891',
        email: 'samy@abc.com',
        password: 'xyz123',
        courseId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Members', null, {});
  }
};
